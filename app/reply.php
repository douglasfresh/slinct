<?php
	if(!$no_messages) { ?>
		<form action="mail.php" method="get">
			<div class="input-append">
				<div class="input-group">
			      <input type="text" name="message" class="form-control" placeholder="You've been Slinct!">
			      <input type="hidden" name="reply" value="<?php echo $reply;?>">
			      <span class="input-group-btn">
			        <button class="btn btn-default" type="submit">Submit</button>
			      </span>
			    </div>
			</div>
		</form>
<?php
	}
	else {
		echo "<div class='alert alert-danger'>You were blocked.</div>";
	}
?>