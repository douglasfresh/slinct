<?php
	session_start();

	if(isset($_SESSION['phone'])) {
		$phone = $_SESSION['phone'];
	}
	else {
		header('Location: login.php');
	}

	include 'config.php';

	$query = "SELECT * FROM chat";
	$result = mysqli_query($con,$query);
	$error = $_GET['error'];

	//Check for existing chats
	while($row = mysqli_fetch_array($result)) {
		if($row['to'] == $phone) {
			$have_to = true;
		}

		if($row['from'] == $phone) {
			$have_from = true;
		}
	}
?>
<html>
<head>
	<title>Messages | Slinct</title>

	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" href="http://slinct.com/assets/img/favicon.png">

	<!-- Bootstrap core CSS -->
    <link href="../assets/css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../assets/css/main.css" rel="stylesheet">

    <!-- Fonts from Google Fonts -->
	<link href='http://fonts.googleapis.com/css?family=Lato:300,400,900' rel='stylesheet' type='text/css'>

	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
    <!-- Fixed navbar -->
    <div class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="../index.php"><b>slinct</b></a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
          	<li><a href="../index.php">Home</a></li>
            <li><a href="logout.php">Sign Out</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>

	<div class="wrapper">
		<?php 
			if($error != null) {
				echo "<div class='spacer'></div><div class='alert alert-danger'>" . $error . "</div>";
			}
		?>

		<div class="spacer"></div>

		<?php 
			if(!$have_to && !$have_from) {
				echo "<div class='alert alert-info'><strong>Welcome to Slinct!</strong> Enter a phone number and a message below to get started.</div>";
			}

			include('send.php'); 
		?>

		<div class="spacer"></div>
<!--		<table class="table">
			<thead>
				<tr>
					<th>Received Chats</th>
					<th></th>	
				</tr>
			</thead>
			<tbody>
				<?php
					if($have_to) {
						$query = "SELECT * FROM chat";
						$result = mysqli_query($con,$query);
						while($row = mysqli_fetch_array($result)) {
							if($row['to'] == $phone) {
								$from = $row['from'];
								$to = $row['to'];
								echo "<tr>";
							  	echo "<td><a href='chat.php?id=" . $row['cid'] . "''>Anonymous: " . $row['from_message'] . "</a></td>";
							  	echo "<td><p class='text-right'><a href='block.php?phone=$from'><span class='glyphicon glyphicon-remove'></span></p></td>";
								echo "</tr>";
							}
						}
					}
					else { echo "<tr><td>No recieved messages</td><td></td></tr>"; }
				?>
			</tbody>
		</table>
		<table class="table">
			<thead>
				<tr>
					<th>Inititated Chats</th>
					<th></th>	
				</tr>
			</thead>
			<tbody>
				<?php
					if($have_from) {
						$query = "SELECT * FROM chat";
						$result = mysqli_query($con,$query);
						while($row = mysqli_fetch_array($result)) {
							if($row['from'] == $phone) {
								$from = $row['from'];
								$to = $row['to'];
								echo "<tr>";
							  	echo "<td><a href='chat.php?id=" . $row['cid'] . "''>You: " . $row['from_message'] . "</a></td>";
							  	echo "<td><p class='text-right'><a href='block.php?phone=$to'><span class='glyphicon glyphicon-remove'></span></p></td>";
								echo "</tr>";
							}
						}
					}
					else { echo "<tr><td>No sent messages</td><td></td></tr>"; }
				?>
			</tbody>
		</table> -->
		<div id="chats"><?php include('chats.php');?></div>
	</div>
	<!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="../assets/js/bootstrap.min.js"></script>
    <script type="text/javascript">

		$(document).ready(function () {
			var interval = setInterval(function() {
				$.ajax({
					url:'chats.php?phone=<?php echo $phone;?>',
					success:function(data){
						$('#chats').html(data);
					}
				});
			}, 1000);
		});

	</script>
</body>
</html>