<?php
	session_start();

	include 'config.php';

	$id = $_GET['id'];

	// Check connection
	if (mysqli_connect_errno())
	  {
	  echo "Failed to connect to MySQL: " . mysqli_connect_error();
	  }

	$result = mysqli_query($con,"SELECT * FROM chat WHERE cid = $id");

	while($row = mysqli_fetch_array($result)) {
	  	$id = $row['cid'];
	  	$from = $row['from'];
	  	$to = $row['to'];

	  	if($_SESSION['phone'] == $row['from']) {
			$from_message = $row['from_message'];
	  		$to_message = $row['to_message'];
	  		$reply = $row['to'];
	  	}
	  	else if($_SESSION['phone'] == $row['to']) {
			$from_message = $row['to_message'];
	  		$to_message = $row['from_message']; 
	  		$reply = $row['from']; 
	  	}
	}
?>
<?php

if($to_message != NULL) {
?>
	<div class="panel panel-default">
		<div class="panel-heading">Anonymous</div>
		<div class="panel-body"><?php echo $to_message; ?></div>
	</div>
<?php
}

if($from_message != NULL) {
?>
	<div class="panel panel-primary">
		<div class="panel-heading">You</div>
		<div class="panel-body"><?php echo $from_message; ?></div>
	</div>
<?php
}

$no_messages= false;

if($from_message == NULL && $to_message ==NULL) {
	$no_messages = true;
}

include 'reply.php';

?>