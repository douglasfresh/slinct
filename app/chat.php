<?php
	session_start();

	if($_SESSION['phone'] == null) {
	    header('Location: ../index.php');
	}

	include 'config.php';

	$id = $_GET['id'];
	$found = false;

	// Check connection
	if (mysqli_connect_errno())
	  {
	  echo "Failed to connect to MySQL: " . mysqli_connect_error();
	  }

	$result = mysqli_query($con,"SELECT * FROM chat WHERE cid = $id");

	while($row = mysqli_fetch_array($result)) {
	  	$cid = $row['cid'];

	  	if($cid == $id) { $found = true;}

	  	$from = $row['from'];
	  	$to = $row['to'];

	  	if($_SESSION['phone'] == $row['from']) {
			$from_message = $row['from_message'];
	  		$to_message = $row['to_message'];
	  		$reply = $row['to'];
	  	}
	  	else if($_SESSION['phone'] == $row['to']) {
			$from_message = $row['to_message'];
	  		$to_message = $row['from_message']; 
	  		$reply = $row['from']; 
	  	}
	}

	if($_SESSION['phone'] != $to && $_SESSION['phone'] != $from) {
	    if($_SESSION['phone'] != null) {
	    	header('Location: index.php');
	    }
	    else {
	    	header('Location: login.php');
	    }
	}

	if(!$found) {
		header('Location: index.php');
	}

	if(isset($_GET['json'])) {
		$array = array();
		$array['from'] = $from;
		$array['to'] = $to;
		$array['from_message'] = $from_message;
		$array['to_message'] = $to_message;
		echo json_encode($array);
		die();
	}
?>
<html>
<head>
	<title>Chat #<?php echo $_GET['id']; ?> | Slinct</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="icon" type="image/png" href="http://slinct.com/assets/img/favicon.png">

	<!-- Bootstrap core CSS -->
    <link href="../assets/css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../assets/css/main.css" rel="stylesheet">

    <!-- Fonts from Google Fonts -->
	<link href='http://fonts.googleapis.com/css?family=Lato:300,400,900' rel='stylesheet' type='text/css'>

	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<div class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="../index.php"><b>slinct</b></a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
          	<li><a href="../index.php">Home</a></li>
          	<li><a href="index.php">Messages</a></li>
            <li><a href="logout.php">Sign Out</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>
	<div class="wrapper">
		<div class="spacer"></div>

		<div id="messages"><?php include 'messages.php'; ?></div>

		<a href="index.php"><span style="margin-right:6px;" class="glyphicon glyphicon-chevron-left"></span>Back to Messages</a>
	</div>

	<!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="../assets/js/bootstrap.min.js"></script>
    <script type="text/javascript">

		$(document).ready(function () {
			var interval = setInterval(function() {
				$.ajax({
					url:'messages.php?id=<?php echo $id;?>',
					success:function(data){
						$('#messages').html(data);
					}
				});
			}, 1000);
		});

	</script>
</body>
</html>
<?php
	mysqli_close($con);
?>