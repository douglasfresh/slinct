<?php

  include 'config.php';

  $error = $_GET['error'];
  $query = "SELECT * FROM user";
  $result = mysqli_query($con,$query);

  //Check for matches
  while($row = mysqli_fetch_array($result)) {
    if($row['phone'] == $_POST['phone']) {
      $exist = true;
      header('Location: register.php?error=Error: ' . $_POST['phone'] . ' already exists');
    }
  }

  if(!$exist && $_POST['phone'] != null) {
    $pass = rand(1,1000000);
    $query = "INSERT INTO `user`(`phone`, `password`) VALUES ('".$_POST['phone']."','$pass')";
    $result = mysqli_query($con,$query);

    if($result) {
      $success = true;
      $to = $_POST['phone'] . "@vtext.com, " . $_POST['phone'] . "@tmomail.net, " . $_POST['phone'] . "@txt.att.net, " . $_POST['phone'] . "@messaging.sprintpcs.com";
      mail($to,"","Thank you for using Slinct! Your temporary password is: " . $pass . ". - http://slinct.com/app/login.php",$headers);
    }
    else {
      //session_start();
      //$_SESSION['phone'] = $_POST['phone'];
      //header("Location: index.php");
    }
  }

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Register | Slinct</title>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" type="image/png" href="http://slinct.com/assets/img/favicon.png">

  <!-- Bootstrap core CSS -->
    <link href="../assets/css/bootstrap.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="../assets/css/main.css" rel="stylesheet">

  <!-- Fonts from Google Fonts -->
  <link href='http://fonts.googleapis.com/css?family=Lato:300,400,900' rel='stylesheet' type='text/css'>

  <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
  <div class="navbar navbar-default navbar-fixed-top">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="../index.php"><b>slinct</b></a>
      </div>
      <div class="navbar-collapse collapse">
        <ul class="nav navbar-nav navbar-right">
          <li><a href="../index.php">Home</a></li>
          <li><a href="login.php">Sign In</a></li>
        </ul>
      </div><!--/.nav-collapse -->
    </div>
  </div>
<div class="container" style="margin-top:70px">
  <div class="col-md-4 col-md-offset-4">
    <?php

      if(isset($error)) {
        echo "<div class='alert alert-danger'>" . $error . "</div>";
      }
      else if($success) {
        echo "<div class='alert alert-success'>A temporary password has been sent to your phone. Once you have received the temporary password, please <strong><a href='login.php'>Sign In</a></strong>.</div>";
      }

    ?>
    <div class="panel panel-default">
      <div class="panel-heading"><h3 class="panel-title"><strong>Register</strong></h3></div>      
      <div class="panel-body">
        <form role="form" action="register.php" method="post">
          <div class="form-group">
            <label for="exampleInputEmail1">Phone</label>
            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="XXX-XXX-XXXX" name="phone">
          </div>
          <button type="submit" class="btn btn-warning btn-lg">Register</button>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="../assets/js/bootstrap.min.js"></script>
</body>
</html>