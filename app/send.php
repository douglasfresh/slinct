<div class="panel panel-default">
  <div class="panel-heading"><h3 class="panel-title"><strong>Compose</strong></h3></div>      
  <div class="panel-body">
    <form role="form" action="mail.php" method="get">
      <div class="form-group">
        <input id="phone" type="tel" name="to" class="form-control" placeholder="Phone Number">
		<input style="margin-top:5px;" id="message" type="text" name="message" class="form-control" placeholder="Message">
      </div>
      <button type="submit" class="btn btn-warning btn-lg">Send</button>
    </form>
  </div>
</div>