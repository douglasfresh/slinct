<?php
	session_start();

	if(isset($_SESSION['phone'])) {
		$phone = $_SESSION['phone'];
	}
	else {
		header('Location: login.php');
	}

	include 'config.php';

	$query = "SELECT * FROM chat";
	$result = mysqli_query($con,$query);
	$error = $_GET['error'];

	//Check for existing chats
	while($row = mysqli_fetch_array($result)) {
		if($row['to'] == $phone) {
			$have_to = true;
		}

		if($row['from'] == $phone) {
			$have_from = true;
		}
	}
?>
		<table class="table">
			<thead>
				<tr>
					<th>Received Chats</th>
					<th></th>	
				</tr>
			</thead>
			<tbody>
				<?php
					if($have_to) {
						$query = "SELECT * FROM chat";
						$result = mysqli_query($con,$query);
						while($row = mysqli_fetch_array($result)) {
							if($row['to'] == $phone) {
								$from = $row['from'];
								$to = $row['to'];
								echo "<tr>";
							  	echo "<td><a href='chat.php?id=" . $row['cid'] . "''>Anonymous: " . $row['from_message'] . "</a></td>";
							  	echo "<td><p class='text-right'><a href='block.php?phone=$from'><span class='glyphicon glyphicon-remove'></span></p></td>";
								echo "</tr>";
							}
						}
					}
					else { echo "<tr><td>No recieved messages</td><td></td></tr>"; }
				?>
			</tbody>
		</table>
		<table class="table">
			<thead>
				<tr>
					<th>Inititated Chats</th>
					<th></th>	
				</tr>
			</thead>
			<tbody>
				<?php
					if($have_from) {
						$query = "SELECT * FROM chat";
						$result = mysqli_query($con,$query);
						while($row = mysqli_fetch_array($result)) {
							if($row['from'] == $phone) {
								$from = $row['from'];
								$to = $row['to'];
								echo "<tr>";
							  	echo "<td><a href='chat.php?id=" . $row['cid'] . "''>You: " . $row['from_message'] . "</a></td>";
							  	echo "<td><p class='text-right'><a href='block.php?phone=$to'><span class='glyphicon glyphicon-remove'></span></p></td>";
								echo "</tr>";
							}
						}
					}
					else { echo "<tr><td>No sent messages</td><td></td></tr>"; }
				?>
			</tbody>
		</table>
<?php mysqli_close($con); ?>