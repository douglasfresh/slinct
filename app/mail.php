<?php
	session_start();

	include 'config.php';

	if(isset($_GET['to'])) {
		$to = $_GET['to'];
	}
	else if(isset($_GET['reply'])) {
		$to = $_GET['reply'];
	}
	else {
		header('Location: index.php');
	}

	if($_GET['message'] == NULL) {
		header('Location: index.php');
	}

	$message = str_replace("'", "\'", $_GET['message']);
	$subject = "";
	$from = $_SESSION['phone'];
	$to_carrier = $to . "@vtext.com, " . $to . "@tmomail.net, " . $to . "@txt.att.net, " . $to . "@messaging.sprintpcs.com";

	$query = "SELECT * FROM chat";
	$result = mysqli_query($con,$query);

	//Check for matches
	while($row = mysqli_fetch_array($result)) {
		if($row['to'] == $to && $row['from'] == $from) {
			$is_from = true;
			$has_chat = true;
			$id = $row['cid'];
		}
		else if($row['to'] == $from && $row['from'] == $to) {
			$has_chat = true;
			$id = $row['cid'];
		}
	}

	$query = "SELECT * FROM block";
	$result = mysqli_query($con,$query);

	//Check for blocks
	while($row = mysqli_fetch_array($result)) {
		if($row['from'] == $from) {
			$blocks = array();
			array_push($blocks, $row['to']);
		}
	}

	$blocked = false;

	if(is_array($blocks)) {
		foreach ($blocks as $block) {
		    if($block == $to) {
		    	$blocked = true;
		    }
		}
	}

	if(!$blocked) {
		if($has_chat) {
			if($is_from) {
				$query = "UPDATE `chat` SET `from_message`='$message' WHERE cid = $id";
			}
			else {
				$query = "UPDATE `chat` SET `to_message`='$message' WHERE cid = $id";
			}
		}
		else {
			$query = "INSERT INTO `chat`(`from`, `to`, `from_message`, `to_message`) VALUES ('".$from."','".$to."','".$message."','')";
		}

		$result=mysqli_query($con,$query);

		if(!$has_chat) {
			$id = mysqli_insert_id($con);
		}

		$text_message =  $_GET['message'] . " - Reply: http://slinct.com/app/chat.php?id=" . $id;
		mail($to_carrier, $subject, $text_message, $headers);

		header('Location: chat.php?id=' . $id);
	}
	else {
		header('Location: index.php?error=Error: You were blocked by ' . $to);
	}

	/*
	echo "From: " . $from . "<br>";
	echo "To: " . $phone . "<br>";
	echo "From Message: " . $from_message . "<br>";
	echo "To Message: " . $to_message . "<br>";
	echo "ID: " . $id;
	echo $query . "<br>";
	

	if($result) {
		header('Location: chat.php?id=' . $id);
	}
	else
		header('Location: index.php?error=Message failed to send');
	*/
?>