<?php
  session_start();

  if($_SESSION['phone'] != null) {
    header('Location: index.php');
  }


  include 'config.php';

  $phone = $_POST['phone'];
  $pass = $_POST['password'];

  // Check connection
  if (mysqli_connect_errno())
    {
    echo "Failed to connect to MySQL: " . mysqli_connect_error();
    }

  $result = mysqli_query($con,"SELECT * FROM user WHERE phone = $phone");

  if(!$result) {
  }
  else {
    while($row = mysqli_fetch_array($result)) {
      if($row['password'] == $pass) {
        $_SESSION['phone']=$phone;
        header( 'Location: index.php');
      }
    }
  }
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Sign In | Slinct</title>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" type="image/png" href="http://slinct.com/assets/img/favicon.png">

  <!-- Bootstrap core CSS -->
  <link href="../assets/css/bootstrap.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="../assets/css/main.css" rel="stylesheet">

  <!-- Fonts from Google Fonts -->
  <link href='http://fonts.googleapis.com/css?family=Lato:300,400,900' rel='stylesheet' type='text/css'>

  <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
    <div class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="../index.php"><b>slinct</b></a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="../index.php">Home</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>
<div class="container" style="margin-top:70px">
  <div class="col-md-4 col-md-offset-4">
    <div class="panel panel-default">
      <div class="panel-heading"><h3 class="panel-title" style="display:inline-block;"><strong>Sign in </strong></h3><div style="float:right;"><a href="register.php">Register</a></div></div>      <div class="panel-body">
        <form role="form" action="login.php" method="post">
          <div class="form-group">
            <label for="exampleInputEmail1">Phone</label>
            <input type="tel" class="form-control" id="exampleInputEmail1" placeholder="XXX-XXX-XXXX" name="phone">
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1">Password</label>
            <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" name="password">
          </div>
          <button type="submit" class="btn btn-warning btn-lg">Sign In</button>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="../assets/js/bootstrap.min.js"></script>

</body>
</html>